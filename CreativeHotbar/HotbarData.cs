using Vintagestory.API.Common;

namespace CreativeHotbar
{
    public class HotbarData
    {
        public int CurrentHotbar { get; set; }
        public HotbarItem[][] HotbarInventory { get; set; } = new HotbarItem[10][];

    }
}