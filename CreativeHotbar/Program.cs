using System;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Datastructures;
using Vintagestory.API.Server;
using Vintagestory.Client.NoObf;
using Vintagestory.Server;

namespace CreativeHotbar
{
    public class CreativeHotbar : ModSystem
    {
        private ICoreServerAPI _sapi;
        private ICoreClientAPI _capi;

        public override void StartServerSide(ICoreServerAPI api)
        {
            _sapi = api;
            
            api.ChatCommands.Create("swhb")
                .RequiresPrivilege(Privilege.gamemode)
                .RequiresPlayer()
                .WithDescription("Switch between 0-9 hotbar contents[0-9, next, prev]")
                .WithArgs(api.ChatCommands.Parsers.Word("command_or_number"))
                .HandleWith(OnSwitchHotbar);

        }

        public override void StartClientSide(ICoreClientAPI api)
        {
            _capi = api;
            
            api.ChatCommands.Create("suinv")
                .RequiresPrivilege(Privilege.gamemode)
                .RequiresPlayer()
                .WithDescription("Open the survival inventory")
                .HandleWith(OnSurvivlaInventory);
        }

        private TextCommandResult OnSurvivlaInventory(TextCommandCallingArgs args)
        {
            if(_capi.World.Player.WorldData.CurrentGameMode != EnumGameMode.Creative)
                return TextCommandResult.Error("You need to be in creative mode to use this command!");
            
            var guiDialogInventory = _capi.Gui.LoadedGuis.First(gui => gui is GuiDialogInventory);
            if (guiDialogInventory.IsOpened())
            {
                guiDialogInventory.TryClose();
            }
            else
            {
                guiDialogInventory.TryOpen();
                var composeSurvivalInvDialog =
                    typeof(GuiDialogInventory).GetMethod("ComposeSurvivalInvDialog",
                        BindingFlags.NonPublic | BindingFlags.Instance);
                if (composeSurvivalInvDialog != null) composeSurvivalInvDialog.Invoke(guiDialogInventory, null);

                var fieldInfos =
                    typeof(GuiDialogInventory).GetField("survivalInvDialog",
                        BindingFlags.NonPublic | BindingFlags.Instance);
                if (fieldInfos != null)
                    guiDialogInventory.Composers["maininventory"] =
                        (GuiComposer)fieldInfos.GetValue(guiDialogInventory);
            }

            return TextCommandResult.Success();
        }

        private TextCommandResult OnSwitchHotbar(TextCommandCallingArgs args)
        {
            var player = args.Caller.Player as ServerPlayer;
            var popword = args[0] as string;
            int popInt;
            HotbarData hotbarData;
            if (player.ServerData.CustomPlayerData.TryGetValue("CreativeHotbarData", out var hotbarDatas))
            {
                hotbarData = JsonConvert.DeserializeObject<HotbarData>(hotbarDatas);
            }
            else
            {
                hotbarData = new HotbarData();
            }

            switch (popword)
            {
                case "next":
                {
                    if (hotbarData.CurrentHotbar + 1 > 9)
                    {
                        popInt = 0;
                    }
                    else
                    {
                        popInt = (hotbarData.CurrentHotbar + 1) % 10;
                    }

                    break;
                }
                case "prev":
                {
                    if (hotbarData.CurrentHotbar - 1 < 0)
                    {
                        popInt = 9;
                    }
                    else
                    {
                        popInt = (hotbarData.CurrentHotbar - 1) % 10;
                    }

                    break;
                }
                default:
                {
                    int.TryParse(popword, out popInt);
                    popInt = Math.Max(0, Math.Min(9, popInt));
                    break;
                }
            }

            if (popInt == hotbarData.CurrentHotbar)
                return TextCommandResult.Success($"Switching to hotbar: {popInt}");

            if (hotbarData.HotbarInventory[hotbarData.CurrentHotbar] is null)
                hotbarData.HotbarInventory[hotbarData.CurrentHotbar] = new HotbarItem[10];
            SaveToCrHotbar(player, hotbarData.HotbarInventory[hotbarData.CurrentHotbar]);
            hotbarData.CurrentHotbar = popInt;

            if (hotbarData.HotbarInventory[hotbarData.CurrentHotbar] is null)
                hotbarData.HotbarInventory[hotbarData.CurrentHotbar] = new HotbarItem[10];
            LoadToPlayerHotbar(player, hotbarData.HotbarInventory[hotbarData.CurrentHotbar]);

            player.ServerData.CustomPlayerData["CreativeHotbarData"] = JsonConvert.SerializeObject(hotbarData);
            return TextCommandResult.Success($"Switching to hotbar: {popInt}");
        }

        public void SaveToCrHotbar(IServerPlayer player, HotbarItem[] hotbarItems)
        {
            var inventory = player.InventoryManager.GetHotbarInventory();
            for (var i = 0; i < inventory.Count; i++)
            {
                if (inventory[i].GetType() == typeof(ItemSlotSurvival))
                {
                    if (inventory[i].Itemstack != null)
                    {
                        var enumItemClass = inventory[i].Itemstack.Class;
                        var stackSize = inventory[i].Itemstack.StackSize;
                        var code = inventory[i].Itemstack.Collectible.Code;
                        var attributes = inventory[i].Itemstack.Attributes as TreeAttribute;

                        hotbarItems[i] = new HotbarItem(enumItemClass, code, stackSize, attributes);
                    }
                    else
                    {
                        hotbarItems[i] = null;
                    }
                }
            }
        }


        public void LoadToPlayerHotbar(IServerPlayer player, HotbarItem[] hotbarItems)
        {
            var inventory = player.InventoryManager.GetHotbarInventory();
            for (var i = 0; i < hotbarItems.Length; i++)
            {
                inventory[i].Itemstack = null;
                inventory[i].MarkDirty();
            }
            for (var i = 0; i < hotbarItems.Length; i++)
            {
                if (hotbarItems[i] is null) continue;
                var asset = new AssetLocation(hotbarItems[i].Code.ToString());
                switch (hotbarItems[i].Itemclass)
                {
                    case EnumItemClass.Item:
                    {
                        var item = _sapi.World.GetItem(asset);

                        if (item != null)
                        {
                            var itemStack = new ItemStack(item, hotbarItems[i].Stacksize)
                            {
                                Attributes = TreeAttribute.CreateFromBytes(hotbarItems[i].Attributes)
                            };

                            inventory[i].Itemstack = itemStack;
                            inventory[i].MarkDirty();
                        }

                        break;
                    }
                    case EnumItemClass.Block:
                    {
                        var block = _sapi.World.GetBlock(asset);
                        if (block != null)
                        {
                            var itemStack = new ItemStack(block, hotbarItems[i].Stacksize)
                            {
                                Attributes = TreeAttribute.CreateFromBytes(hotbarItems[i].Attributes)
                            };

                            inventory[i].Itemstack = itemStack;
                            inventory[i].MarkDirty();
                        }

                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}